#!/bin/sh
GITLAB_REGISTRY='registry.gitlab.com/prabin_sapkota/dotnet-sample'
TAGS='3.0'


docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t "${GITLAB_REGISTRY}":"${TAGS}" .
docker push "${GITLAB_REGISTRY}":"${TAGS}"


# docker push registry.gitlab.com/prabin_sapkota/dotnet-sample:2.0
