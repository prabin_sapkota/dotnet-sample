#building the image using dotnet sdk
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 as build
WORKDIR /webapp
COPY . .

RUN dotnet restore
RUN dotnet publish -c release -o /app

#building the image using dotnet runtime

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build /app ./
EXPOSE 80
CMD ["dotnet", "webapp.dll"]








# # builds our image using dotnet's sdk
# FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
# WORKDIR /source
# COPY . ./webapp/
# WORKDIR /source/webapp
# RUN dotnet restore
# RUN dotnet publish -c release -o /app --no-restore

# # runs it using aspnet runtime
# FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
# WORKDIR /app
# COPY --from=build /app ./
# ENTRYPOINT ["dotnet", "webapp.dll"]
